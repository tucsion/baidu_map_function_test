/*
 * @Author: 郑博 
 * @Date: 2018-06-26 01:39:37 
 * @Last Modified by:   郑博 
 * @Last Modified time: 2018-06-26 01:39:37 
 */


//创建Map实例
function createMap() {
    map = new BMap.Map("map", { minZoom: 11, maxZoom: 19 });
    map.centerAndZoom('北京', 12);// 初始化地图,设置中心点坐标和地图级别。
}

// 设置地图事件
function setMapEvent() {
    map.enableScrollWheelZoom();//启用地图滚轮放大缩小
    map.enableKeyboard();//启用键盘上下左右键移动地图
    map.enableDragging(); //启用地图拖拽事件，默认启用(可不写)
    map.enableDoubleClickZoom()//启用鼠标双击放大，默认启用(可不写)
}

//向地图添加控件
function addMapControl() {
    var scaleControl = new BMap.ScaleControl({
        anchor: BMAP_ANCHOR_BOTTOM_LEFT
    });
    scaleControl.setUnit(BMAP_UNIT_IMPERIAL);
    map.addControl(scaleControl);

    var navControl = new BMap.NavigationControl({
        anchor: BMAP_ANCHOR_TOP_LEFT,
        type: BMAP_NAVIGATION_CONTROL_LARGE
    });
    map.addControl(navControl);

    var overviewControl = new BMap.OverviewMapControl({
        anchor: BMAP_ANCHOR_BOTTOM_RIGHT,
        isOpen: true
    });
    map.addControl(overviewControl);
}


// 添加覆盖物
function addMapOverlay() {
    map.addEventListener('zoomend', function (e) { // 根据缩放的级别不同展示不同的内容
        var zoom = map.getZoom()
        if (zoom >= 11 && zoom <= 13) {
            areaShow()
        } else if (zoom > 13 && zoom <= 15) {
            plateShow()
        } else if (zoom > 15) {
            communityShow()
        }
    })
}


/*===================================初始化========================================*/
//创建和初始化地图函数：
function initMap() {
    createMap();//创建地图
    setMapEvent();//设置地图事件
    addMapControl();//向地图添加控件
    addMapOverlay();//向地图添加覆盖物
}



/*===================================交互========================================*/


// 点击事件
function attribute(e) {
    var ePoint = e.currentTarget.point
    var point = new BMap.Point(ePoint.lng, ePoint.lat)
    var zoom = map.getZoom()
    if (zoom >= 11 && zoom <= 13) {
        plateShow()
        map.centerAndZoom(point, 14)
    } else if (zoom > 13 && zoom <= 15) {
        communityShow()
        map.centerAndZoom(point, 16)
    } else if (zoom > 15) {
        alert('小区级别')
    }

    console.log(map.getZoom())

}


// 数据处理
var areaMarkers = []
var plateMarkers = []
var communityMarkers = []

for (var i = 0; i < areas.length; i++) {
    var item = areas[i];
    var dataPoint = item.position.split(',')
    var point = new BMap.Point(dataPoint[0], dataPoint[1])
    var opts = {
        position: point,    // 指定文本标注所在的地理位置
        offset: new BMap.Size(30, -30)    //设置文本偏移量
    }
    var item = new BMap.Label("<div class='circular' data-level='1'><div class='circular-container'><p class='areaName'>" + item.areaName + "</p><p class='count'>" + item.propertyCount + "套</p></div></div>", opts)
    item.addEventListener("click", attribute);
    areaMarkers.push(item)
}

for (var i = 0; i < plates.length; i++) {
    var item = plates[i];
    var dataPoint = item.position.split(',')
    var point = new BMap.Point(dataPoint[0], dataPoint[1])
    var opts = {
        position: point,    // 指定文本标注所在的地理位置
        offset: new BMap.Size(30, -30)    //设置文本偏移量
    }
    var item = new BMap.Label("<div class='circular circularPlate'><div class='circular-container'><p class='areaName'>" + item.plateName + "</p><p class='count'>" + item.propertyCount + "套</p></div></div>", opts)
    item.addEventListener("click", attribute);
    plateMarkers.push(item)
}

for (var i = 0; i < communitys.length; i++) {
    var item = communitys[i];
    var dataPoint = item.position.split(',')
    var point = new BMap.Point(dataPoint[0], dataPoint[1])
    var opts = {
        position: point,    // 指定文本标注所在的地理位置
        offset: new BMap.Size(30, -30)    //设置文本偏移量
    }
    var item = new BMap.Label("<div class='box'><span class='name'>" + item.communityName + "</span><span class='count'>" + item.propertyCount + "套</span></div>", opts)
    item.addEventListener("click", attribute);
    communityMarkers.push(item)
}


function areaShow() {
    for (var i = 0; i < areaMarkers.length; i++) {
        var item = areaMarkers[i]
        map.addOverlay(item)
        item.show()
    }
    for (var i = 0; i < plateMarkers.length; i++) {
        var item = plateMarkers[i]
        map.addOverlay(item)
        item.hide()
    }
    for (var i = 0; i < communityMarkers.length; i++) {
        var item = communityMarkers[i]
        map.addOverlay(item)
        item.hide()
    }
}

function plateShow() {
    for (var i = 0; i < areaMarkers.length; i++) {
        var item = areaMarkers[i]
        map.addOverlay(item)
        item.hide()
    }
    for (var i = 0; i < plateMarkers.length; i++) {
        var item = plateMarkers[i]
        map.addOverlay(item)
        item.show()
    }
    for (var i = 0; i < communityMarkers.length; i++) {
        var item = communityMarkers[i]
        map.addOverlay(item)
        item.hide()
    }
}

function communityShow() {
    for (var i = 0; i < areaMarkers.length; i++) {
        var item = areaMarkers[i]
        map.addOverlay(item)
        item.hide()
    }
    for (var i = 0; i < plateMarkers.length; i++) {
        var item = plateMarkers[i]
        map.addOverlay(item)
        item.hide()
    }
    for (var i = 0; i < communityMarkers.length; i++) {
        var item = communityMarkers[i]
        map.addOverlay(item)
        item.show()
    }
}


