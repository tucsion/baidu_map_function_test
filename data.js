/*====================Markers=====================*/
var areas = [
    {
        areaName: '昌平区',
        propertyCount: 5662,
        position: '116.235891, 40.226854'
    },
    {
        areaName: '海淀区',
        propertyCount: 2345,
        position: '116.303588, 39.96535',
    },
    {
        areaName: '西城',
        propertyCount: 1930,
        position: '116.358716,39.920117'
    }
]

var plates = [
    {
        plateName: '紫竹桥',
        propertyCount: 156,
        position: '116.319806,39.949165'
    },
    {
        plateName: '左安门',
        propertyCount: 74,
        position: '116.44115,39.876583'
    },
    {
        plateName: '陶然亭',
        propertyCount: 157,
        position: '116.386245,39.88464'
    },
    {
        plateName: '六里桥',
        propertyCount: 244,
        position: '116.314003,39.890826'
    }
]

var communitys = [
    {
        communityName: '莲怡园二区',
        propertyCount: 25,
        position: '116.303421,39.89174'
    },
    {
        communityName: '宝莲路10号院',
        propertyCount: 4,
        position: '116.303781,39.894632'
    },
    {
        communityName: '靛厂路六号院',
        propertyCount: 15,
        position: '116.299415,39.894494'
    },
    {
        communityName: '海丰家园',
        propertyCount: 24,
        position: '116.297475,39.897691'
    }
]