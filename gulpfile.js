let gulp = require('gulp')
let connect = require('gulp-connect')
let changed = require('gulp-changed')
let less = require('gulp-less')

let SRC = './'


gulp.task('html', function () {
    gulp.src(SRC + '/*.html')
        .pipe(connect.reload())
})

gulp.task('less', function () {
    gulp.src([SRC + '/*.less'])
        .pipe(less())
        .pipe(gulp.dest(SRC + '/'))
        .pipe(connect.reload())
})


gulp.task('js', function () {
    gulp.src(SRC + '/*.js')
        .pipe(connect.reload())
})

gulp.task('connect', function () {
    connect.server({
        //host: '192.168.1.110', //地址，可不写，不写的话，默认localhost
        port: 8080, //端口号，可不写，默认8000
        root: SRC, //当前项目主目录
        livereload: true, //自动刷新
        host: '::'
    })
})

gulp.task('watch', function () {
    gulp.watch(SRC + '/*.less', ['less'])
    gulp.watch(SRC + '/*.js', ['js'])
    gulp.watch([SRC + '/*.html'], ['html'])
})

gulp.task('default', ['watch', 'connect'])
